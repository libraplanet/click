using System;
using System.Windows;
using System.Windows.Forms;
using System.Drawing;

using System.Runtime.InteropServices;

public class Program
{
	/**
	 * http://homepage3.nifty.com/midori_no_bike/CS/userIO.html
	 * の『マウスの自動操作』より。
	 */
	[DllImport("user32.dll")]
	extern static uint SendInput(
		uint       nInputs,   // INPUT 構造体の数(イベント数)
		INPUT[]    pInputs,   // INPUT 構造体
		int        cbSize     // INPUT 構造体のサイズ
		) ;

	[StructLayout(LayoutKind.Sequential)]  // アンマネージ DLL 対応用 struct 記述宣言
	struct INPUT
	{ 
		public int        type ;  // 0 = INPUT_MOUSE(デフォルト), 1 = INPUT_KEYBOARD
		public MOUSEINPUT mi   ;
		// Note: struct の場合、デフォルト(パラメータなしの)コンストラクタは、
		//       言語側で定義済みで、フィールドを 0 に初期化する。
	}

	[StructLayout(LayoutKind.Sequential)]  // アンマネージ DLL 対応用 struct 記述宣言
	struct MOUSEINPUT
	{
		public int    dx ;
		public int    dy ;
		public int    mouseData ;  // amount of wheel movement
		public int    dwFlags   ;
		public int    time      ;  // time stamp for the event
		public IntPtr dwExtraInfo ;
		// Note: struct の場合、デフォルト(パラメータなしの)コンストラクタは、
		//       言語側で定義済みで、フィールドを 0 に初期化する。
	}

	// dwFlags
	const int MOUSEEVENTF_MOVED      = 0x0001 ;
	const int MOUSEEVENTF_LEFTDOWN   = 0x0002 ;  // 左ボタン Down
	const int MOUSEEVENTF_LEFTUP     = 0x0004 ;  // 左ボタン Up
	const int MOUSEEVENTF_RIGHTDOWN  = 0x0008 ;  // 右ボタン Down
	const int MOUSEEVENTF_RIGHTUP    = 0x0010 ;  // 右ボタン Up
	const int MOUSEEVENTF_MIDDLEDOWN = 0x0020 ;  // 中ボタン Down
	const int MOUSEEVENTF_MIDDLEUP   = 0x0040 ;  // 中ボタン Up
	const int MOUSEEVENTF_WHEEL      = 0x0080 ;
	const int MOUSEEVENTF_XDOWN      = 0x0100 ;
	const int MOUSEEVENTF_XUP        = 0x0200 ;
	const int MOUSEEVENTF_ABSOLUTE   = 0x8000 ;

	const int screen_length = 0x10000 ;  // for MOUSEEVENTF_ABSOLUTE (この値は固定)


	[STAThread]
	public static void Main()
	{
		using(Form f = new ClickForm())
		{
			Application.Run(f);
		}
	}
	
	class ClickForm : Form
	{
		public ClickForm ()
		{
			this.SuspendLayout();
			this.ClientSize = new Size(150,60);
			this.Text = "click!";
			this.FormBorderStyle = FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;

			Button button = new Button();
			button.Text = "click!";
			button.Dock = DockStyle.Fill;
			button.Click += new EventHandler(delegate(object sender, EventArgs e)
			{
				INPUT[] input = new INPUT[3];
				input[0].mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
				input[1].mi.dwFlags = MOUSEEVENTF_LEFTUP;
				SendInput(2, input, Marshal.SizeOf(new INPUT()));
			});

			Panel panel = new Panel();
			panel.Dock = DockStyle.Fill;
			panel.Padding = new Padding(10);
			panel.Controls.Add(button);

			this.Controls.Add(panel);
			this.ResumeLayout();
		}
	}
}